# HSGR Doom Box
FPS game runing in CommonsLab's Donation Box

## Dependancies
`freedoom`

## TODO
* Add support for the printer
* Add a second button
* Add a timeout to restart the demo after a donation
* Enable VC4 driver
* Create ansible script

## Licence
GPLv3

## Inspiration
The COOP Box https://github.com/commonslabgr/donation-box
